//
//  main.m
//  Learning Objective C
//
//  Created by Nuno Cancelo on 8/15/13.
//  Copyright (c) 2013 Nuno Cancelo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Fraction.h"
int main(int argc, const char * argv[])
{

    @autoreleasepool {

        NSLog(@"Hello, World!");
        
        Fraction* f1 = [[Fraction alloc] init];
        Fraction* f2 = [[Fraction alloc] init];
        
        f1.numerator=2;
        f1.denominator=3;
        
        [f2 setNumerator:3 andDenominator:4];
        
        [f1 print];
        [f2 print];
        
        [f1 multiply:f2];
        
        [f1 print];
        
        NSLog(@"F1 * F2 = %.2g", [f1 toDouble]);
        
        
    }
    return 0;
}

