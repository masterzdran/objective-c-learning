//
//  Fraction.m
//  Learning Objective C
//
//  Created by Nuno Cancelo on 8/15/13.
//  Copyright (c) 2013 Nuno Cancelo. All rights reserved.
//

#import "Fraction.h"

@implementation Fraction

@synthesize denominator;
@synthesize numerator;

-(double) toDouble
{
    return numerator /(double) denominator;
}

-(void) print
{
    NSLog(@"Fraction = %i / %i",numerator,denominator);
}

-(void) setNumerator:(int)n andDenominator:(int)d
{
    self.numerator=n;
    self.denominator=d;
}

-(void) multiply:(Fraction *)anotherFraction
{
    numerator*=anotherFraction.numerator;
    denominator *= anotherFraction.denominator;
}

@end
