//
//  Fraction.h
//  Learning Objective C
//
//  Created by Nuno Cancelo on 8/15/13.
//  Copyright (c) 2013 Nuno Cancelo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Fraction : NSObject
{
    int numerator;
    int denominator;
}
@property int numerator;
@property int denominator;

- (double) toDouble;
- (void) print;

-(void) setNumerator:(int)numerator andDenominator:(int)denominator;

-(void) multiply:(Fraction*) anotherFraction;

@end
